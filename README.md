#   Pico Heads Shownotes

## Technical Details

    - Cart Filename catfight.p8
    - Git Repo: https://bitbucket.org/zothynine/picoheads.git
    - Git CloneURL: https://zothynine@bitbucket.org/zothynine/picoheads.git
    - Git-Tag: s01e01
    - Player Name ist ASCII

## STATION IDs

    - [] Maria Sterkl
    - [] Oscar Bronner
    - [] Aco
    - [] Gerlinde
    - [] Kotynek
    - [] Koller
    - [] Proschofsky
